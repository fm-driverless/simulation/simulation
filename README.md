# Simulation

The Simulation subsystem targets the prototyping phase of FM_Driverless.
We cannot accomodate every small change made in the design or the model in any of the Driverless modules. So we require a method to rapidly prototype the changes made and to observe how the vehicle shall behave under these changes. To overcome these issues, we simulate the Driverless System on a Simulator. 